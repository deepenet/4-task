CCpp = g++

CFlags = -std=c++17

all: build

build: main.o
	$(CCpp) main.o -o main

main.o: main.cpp
	$(CCpp) $(CFlags) main.cpp -c

clear:
	rm *.o