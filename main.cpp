#include <iostream>
#include <string>
#include "CSVParser.h"

using std::cout;
using std::endl;

int main() {
	std::ifstream file("test.csv");
	try {
		CSVParser<int, long long, std::string, float, double> parser(file, 3);
		for (std::tuple<int, long long, std::string, float, double> rs : parser) {
			cout << rs << endl;
		}
	} catch(invalidArgsType err) {
		cout << err.what() << endl;
	}
}
