


/*	
 *	Данная реализация парсера способна обрабатывать следующие типы:
 *	int, long long, std::string, float, double
 *	
 *	При попытке перевода ячейки в необрабатываемый тип выбросится исключение invalidArgsType
 *	
 *	В случае неудачной попытки привести содержимое ячейки к указанному пользователем типу
 *	вместо вывода строки будет напечатано о выбросе исключения parsingError, после чего
 *	работа продолжится
 *	
 *	В вещественные типы возможно преобразование только с использованием символа '.'
 *	В случае использование другого символа (например, символа ',') обработана будет
 *	только предшествующая символу часть числа
 *
 *	Во время обработки числа в случае наличия внутри него постороннего символа 
 *	обработка завершится на нем и в tuple будет записана только та часть, 
 *	которая предшествовала постороннему символу
*/



#pragma once
#include <memory>
#include <fstream>
#include <string>
#include <tuple>
#include <exception>
#include <typeinfo>
#include <list>

class parsingError : public std::exception {
public:
	const char* what() const noexcept {
		return "ERROR: Line parsing error.";
	}
};

class invalidArgsType : public std::exception {
public:
	const char* what() const noexcept {
		return "ERROR: Some argument has invalid type for parsing.";
	}
};

#include "recursive_template.h"


// Оператор вывода в поток tuple'а
template<typename Ch, typename Tr, typename... Args> 
std::basic_ostream<Ch, Tr>& operator<<(std::basic_ostream<Ch, Tr>& os, std::tuple<Args...> const& t) {
	// Обход элементов кортежа и вызвов обработчика
	discovering_tuple::for_each(os, t, discovering_tuple::printEach());
	return os;
}

// Функция разбора строки, разбитой на ячейки (хранятся в list'е), и записи результата в tuple
template<typename... Args>
void recursive_parse(std::tuple<Args...>& tuple, std::list<std::string>& line) {
	// Обход элементов кортежа и вызвов обработчика
	parseLineToTuple::for_each(tuple, line, parseLineToTuple::parseEach{});
}

template<typename... Args>
class CSVParser {
	std::ifstream const& file;
	std::tuple<Args...> activeTuple;
	class CSVParserIterator: public std::iterator<std::input_iterator_tag, std::tuple<Args...>> {
		// Информация для операции == о конце
		bool isEnd;
		// Поток чтения
		std::ifstream const& file;
		// Ссылка на tuple, в который в данный момент происходит запись
		std::tuple<Args...>& p;
	public:
		CSVParserIterator(bool status, std::ifstream const& file, std::tuple<Args...>& p) : isEnd(status), file(file), p(p) {}
		bool operator!=(CSVParserIterator const& other) const;
		bool operator==(CSVParserIterator const& other) const;
		typename CSVParserIterator::reference operator*() const;
		// Фактически, разбор происходит в данном операторе (operator++)
		// Внутри происходит цикл, который считывает строку и пытается ее разобрать
		// Цикл действует до тех пор, пока не сможет разобрать первую строку или пока строки не закончатся
		// В случае разбора строки запишет ее в поле p
		// В случае конца файла отметит это в поле isEnd
		CSVParserIterator& operator++();
		std::tuple<Args...> getTuple() const;
	};
	
public:
	CSVParser(std::ifstream const& file, int skipLines = 0);
	CSVParserIterator begin();
	CSVParserIterator end();
};

// Функция полного синтаксического разбора стоки
template<typename... Args>
void parseLine(std::tuple<Args...>& input_tuple, std::string const& str) {
	// Контейнер, хранящий ячейки (на них разбивается строка)
	std::list<std::string> cells;
	std::string tmpStr = "";
	int couner = 0;
	const int border = str.size();
	// Разбиение линии на ячейки
	while(couner < border) {
		if(str[couner] == ';') {
			cells.push_back(tmpStr);
			tmpStr = "";
		}
		else {
			tmpStr += str[couner];
		}
		couner++;
	}
	cells.push_back(tmpStr);
	// Рекурсивно пройтись по tuple'у и каждому типу попытаться присвоить преобразованный в нужный тип элемент, иначе исключение ошибки парсинга
	recursive_parse(input_tuple, cells);
	// Если остались незадействованные ячейки, то отчистить
	cells.clear();
}

template<typename... Args>
bool CSVParser<Args...>::CSVParserIterator::operator!=(CSVParserIterator const& other) const {
	return isEnd != other.isEnd;
}

template<typename... Args>
std::tuple<Args...> CSVParser<Args...>::CSVParserIterator::getTuple() const {
	return p;
}

template<typename... Args>
bool CSVParser<Args...>::CSVParserIterator::operator==(CSVParserIterator const& other) const {
	return isEnd == other.isEnd;
}

template<typename... Args>
typename CSVParser<Args...>::CSVParserIterator::reference CSVParser<Args...>::CSVParserIterator::operator*() const {
	return p;
}

template<typename... Args>
typename CSVParser<Args...>::CSVParserIterator& CSVParser<Args...>::CSVParserIterator::operator++() {
	std::string line = "";
	// Считывание строки и ее разбор
	while(true) {
		try {
			if(!std::getline(const_cast<std::ifstream&>(file), line, '\n')) {
				isEnd = true;
				break;
			}
			parseLine(p, line);
			break;
		} catch(parsingError err) {
			std::cout << err.what() << std::endl;
		}
	}
	return *this;
}

template<typename... Args>
CSVParser<Args...>::CSVParser(std::ifstream const& file, int skipLines) : file(file) {
	// Проверка входных типов на наличие реализации их разбора в парсере
	if(!types_analys::isArgsCorrect<Args...>()) throw invalidArgsType{};
	CSVParserIterator tmpIterator{false, file, activeTuple};
	// Пропустить указанное количество строк
	for(int i = 0; i < skipLines; ++i) {
		std::string tmp;
		std::getline(const_cast<std::ifstream&>(file), tmp, '\n');
	}
	// Произвести разбор первой строки (по факту, первой строки, которую вообще возможно полностю разобрать)
	++tmpIterator;
	// Сохранить tuple, полученный из первой допустимой строки
	activeTuple = tmpIterator.getTuple();
}

template<typename... Args>
typename CSVParser<Args...>::CSVParserIterator CSVParser<Args...>::begin() {
	return CSVParserIterator(false, file, activeTuple);
}

template<typename... Args>
typename CSVParser<Args...>::CSVParserIterator CSVParser<Args...>::end() {
	return CSVParserIterator(true, file, activeTuple);
}