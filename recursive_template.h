#pragma once

namespace discovering_tuple {

	struct printEach {
		//Оператор принимает ссылку на поток и ссылку на элемент tuple'а, который необходимо вывести
		template<typename Os, typename T>
		void operator()(Os& os, T&& t) {
			os << t << ";\t";
		}
	};

	// Общая реализация рекурсивного обхода tuple'a
	template<typename Os, int index, typename printEach, typename... Args>
	struct iterate_tuple {
		static void next(Os& os, std::tuple<Args...>const& t, printEach print) {
			// Уменьшаем позицию и рекурсивно вызываем этот же метод 
			iterate_tuple<Os, index - 1, printEach, Args...>::next(os, t, print);
			// Вызываем обработчик и передаем ему поток и значение элемента
			print(os, std::get<index>(t));
		}
	};

	// Частичная специализация для индекса 0 (завершает рекурсию)
	template<typename Os, typename printEach, typename... Args>
	struct iterate_tuple<Os, 0, printEach, Args...> {
		static void next(Os& os, std::tuple<Args...>const& t, printEach print) {
			// Только вызываем обработчик и передаем ему поток и значение элемента
			print(os, std::get<0>(t));
		}
	};

	// Частичная специализация для индекса -1 (пустой кортеж)
	template<typename Os, typename printEach, typename... Args>
	struct iterate_tuple<Os, -1, printEach, Args...> {
		static void next(Os& os, std::tuple<Args...>const& t, printEach print) { /* Ничего не делать */ }
	};

	// Функция запускает рекурсивный обход tuple'а
	template<typename Os, typename printEach, typename... Args>
	void for_each(Os& os, std::tuple<Args...>const& t, printEach print) {
		// Размер кортежа
		int const t_size = std::tuple_size<std::tuple<Args...>>::value;
		// Запускаем рекурсивный обход элементов кортежа во время компиляции
		iterate_tuple<Os, t_size - 1, printEach, Args...>::next(os, t, print);
	}
}

namespace types_analys {

	// Функция проверяет входной тип на наличие реализации приведения типа std::string к нему
	template<typename T>
	bool isArgTypeCorrect(T in) {
		if((typeid(in) == typeid(int)) || (typeid(in) == typeid(std::string)) || (typeid(in) == typeid(float)) || (typeid(in) == typeid(double)) || (typeid(in) == typeid(long long))) {
			return true;
		}
		return false;
	}

	// Общая реализация рекурсивного обхода tuple'a
	template<typename firstType, typename... Args>
	struct isArgsCorrectAnother {
		static bool next() {
			firstType first;
			if(!isArgTypeCorrect(first))
				return false;
			if(sizeof...(Args)) {
				return isArgsCorrectAnother<Args...>::next();
			}
			return true;
		}
	};

	// Частичная специализация для последнего аргумента
	template<typename lastType>
	struct isArgsCorrectAnother<lastType> {
		static bool next() {
			lastType first;
			if(!isArgTypeCorrect(first))
				return false;
			return true;
		}
	};

	// Функция, запускающая анализ типов на допустимость
	template<typename... Args>
	bool isArgsCorrect() {
		if(sizeof...(Args))
			return isArgsCorrectAnother<Args...>::next();
		return true;
	}
}

namespace parseLineToTuple {

	struct parseEach {
		// Перевод типа string в соответствующий тип и запись по ссылке в элемент tuple'а
		template<typename T>
		void operator()(T&& t, std::string const& cell);
	};

	// Оператор перегружен для всех обрабатываемых типов
	// Необходимости вызова данного оператора не будет т.к.
	// Ранее происходит проверка типов на наличие их обработчика
	template<typename T>
	void parseEach::operator()(T&& t, std::string const& cell) {}

	// Перегрузка оператора для int'а
	template<>
	void parseEach::operator()<int&>(int& t, std::string const& cell) {
		char* errptr = nullptr;
		int tmp = static_cast<int>(strtol(cell.c_str(), &errptr, 10));
		if(*errptr == cell[0]) {
			if(!cell.size()) t = 0;
			else throw parsingError{};
		}
		t = tmp;
	}

	// Перегрузка оператора для long long'а
	template<>
	void parseEach::operator()<long long&>(long long& t, std::string const& cell) {
		char* errptr = nullptr;
		long long tmp = (strtoll(cell.c_str(), &errptr, 10));
		if(*errptr == cell[0])
			if(!cell.size()) t = 0;
			else throw parsingError{};
		t = tmp;
	}

	// Перегрузка оператора для string'а
	template<>
	void parseEach::operator()<std::string&>(std::string& t, std::string const& cell) {
		t = cell;
	}

	// Перегрузка оператора для float'а
	template<>
	void parseEach::operator()<float&>(float& t, std::string const& cell) {
		char* errptr = nullptr;
		float tmp = (strtof(cell.c_str(), &errptr));
		if(*errptr == cell[0])
			if(!cell.size()) t = 0;
			else throw parsingError{};
		t = tmp;
	}

	// Перегрузка оператора для double'а
	template<>
	void parseEach::operator()<double&>(double& t, std::string const& cell) {
		char* errptr = nullptr;
		double tmp = (strtod(cell.c_str(), &errptr));
		if(*errptr == cell[0])
			if(!cell.size()) t = 0;
			else throw parsingError{};
		t = tmp;
	}

	// Общая реализация рекурсивного обхода tuple'a
	template<int index, typename parseEach, typename... Args>
	struct iterate_tuple {
		static void next(std::tuple<Args...>& t, std::list<std::string>& line, parseEach parse) {
			// Уменьшаем позицию и рекурсивно вызываем этот же метод 
			iterate_tuple<index - 1, parseEach, Args...>::next(t, line, parse);

			// Вызываем обработчик и передаем ему позицию и значение элемента, после чего удаляем элемент
			if(!line.empty()) {
				parse(std::get<index>(t), line.front());
				line.pop_front();
			}
		}
	};

	// Частичная специализация для индекса 0 (завершает рекурсию)
	template<typename parseEach, typename... Args>
	struct iterate_tuple<0, parseEach, Args...> {
		static void next(std::tuple<Args...>& t, std::list<std::string>& line, parseEach parse) {
			if(!line.empty()) {
				parse(std::get<0>(t), line.front());
				line.pop_front();
			}
		}
	};

	// Частичная специализация для индекса -1 (пустой кортеж)
	template<typename parseEach, typename... Args>
	struct iterate_tuple<-1, parseEach, Args...> {
		static void next(std::tuple<Args...>& t, std::list<std::string>& line, parseEach parse) {}
	};

	// Функция запускает рекурсивный обход tuple'а
	template<typename parseEach, typename... Args>
	void for_each(std::tuple<Args...>& t, std::list<std::string>& line, parseEach parse) {
		// Размер кортежа
		int const t_size = std::tuple_size<std::tuple<Args...>>::value;
		// Запускаем рекурсивный обход элементов кортежа во время компиляции
		iterate_tuple<t_size - 1, parseEach, Args...>::next(t, line, parse);
	}
}